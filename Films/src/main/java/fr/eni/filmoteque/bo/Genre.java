package fr.eni.filmoteque.bo;

public class Genre {
	private Integer id;
	private String libelle;
	
	public Genre() {
		// TODO Auto-generated constructor stub
	}
	
	public Genre(Integer id, String libelle) {
		super();
		this.id=id;
		this.libelle=libelle;
	}

	public Genre(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
}
