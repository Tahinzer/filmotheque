package fr.eni.filmoteque.bo;

public class Realisateur {

	private Integer id;
	private String nom;
	
	public Realisateur() {
		// TODO Auto-generated constructor stub
	}
	
	public Realisateur(Integer id, String nom) {
		this.id=id;
		this.nom=nom;
	}

	public Realisateur(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
