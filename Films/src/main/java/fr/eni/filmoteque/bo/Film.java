package fr.eni.filmoteque.bo;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;

public class Film {
	
	private Integer id;
	@NotBlank
	private String nom;
	
	@Min(1800)
	private Integer annee;
	@NotBlank
	private String resume_;
	
	private Genre genre;
	private Realisateur realisateur;
	
	private int id_genre;
	private int id_realisateur;
	private String bandeAnnonce;
	private String image_;
	
	
	



	


	public Film() {
		// TODO Auto-generated constructor stub
	}

	public Film(Integer id, String nom, Integer annee, String resume, Genre genre, Realisateur realisateur) {
		this.id = id;
		this.nom = nom;
		this.annee = annee;
		this.resume_ = resume;
		this.genre = genre; 
		this.realisateur=realisateur;

	}

	

	public Film(Integer id) {
		this.id=id;
	}

	public Film(String nom, int annee, String resume_, Genre genre, Realisateur realisateur) {
		this.nom=nom;
		this.annee=annee;
		this.resume_=resume_;
		this.genre=genre;
		this.realisateur=realisateur;
	}

	

	public Film(String nom, Integer annee, String resume_,Genre genre,Realisateur realisateur, int id_genre, int id_realisateur) {
		this.nom=nom;
		this.annee=annee;
		this.resume_=resume_;
		this.genre=genre;
		this.realisateur=realisateur;
		this.id_genre=id_genre;
		this.id_realisateur=id_realisateur;
	}
	



	public Film(int id, String nom, int annee, String resume_, Genre genre, Realisateur realisateur, String bandeAnnonce) {
		this.id=id;
		this.nom=nom;
		this.annee=annee;
		this.resume_=resume_;
		this.genre=genre;
		this.realisateur=realisateur;
		this.bandeAnnonce=bandeAnnonce;
	}
	
	public Film(int id, String nom, int annee, String resume_, Genre genre, Realisateur realisateur, String bandeAnnonce, String image_) {
		this.id=id;
		this.nom=nom;
		this.annee=annee;
		this.resume_=resume_;
		this.genre=genre;
		this.realisateur=realisateur;
		this.bandeAnnonce=bandeAnnonce;
		this.image_=image_;
	}

	public Film(String nom, Integer annee, String resume_, Genre genre, Realisateur realisateur, int id_genre,
			int id_realisateur, String bandeAnnonce, String image_) {
		this.nom=nom;
		this.annee=annee;
		this.resume_=resume_;
		this.genre=genre;
		this.realisateur=realisateur;
		this.id_genre= id_genre;
		this.id_realisateur=id_realisateur;
		this.bandeAnnonce=bandeAnnonce;
		this.image_=image_;
	}

	

	

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public String getResume_() {
		return resume_;
	}

	public void setResume_(String resume) {
		this.resume_ = resume;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Realisateur getRealisateur() {
		return realisateur;
	}

	public void setRealisateur(Realisateur realisateur) {
		this.realisateur = realisateur;
	}

	public int getId_genre() {
		return this.getGenre().getId();
	}

	

	public int getId_realisateur() {
		return this.getRealisateur().getId();
	}

	
	public String getBandeAnnonce() {
		return bandeAnnonce;
	}

	public void setBandeAnnonce(String bandeAnnonce) {
		this.bandeAnnonce = bandeAnnonce;
	}
	

	public String getImage_() {
		return image_;
	}

	public void setImage_(String image_) {
		this.image_ = image_;
	}
	
	

	

	



	

}
