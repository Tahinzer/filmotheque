package fr.eni.filmoteque.bll;

import java.util.List;
import org.springframework.stereotype.Service;

import fr.eni.filmoteque.bo.Genre;
import fr.eni.filmoteque.dal.GenreDAO;

@Service("genreService")
public class GenreServiceImpl implements GenreService {

	private GenreDAO genreDAO;
	
	public GenreServiceImpl(GenreDAO genreDAO) {
		this.genreDAO=genreDAO;	
	}
	
	@Override
	public List<Genre> getListeGenre() {
		
		return genreDAO.findAll();
	}

	@Override
	public Genre findGenreById(Integer id) {
		
		return genreDAO.read(id);
	}

}
