package fr.eni.filmoteque.bll;

import java.util.List;

import fr.eni.filmoteque.bo.Genre;

public interface GenreService {

	public List<Genre> getListeGenre();
	
	public Genre findGenreById(Integer id);
	
}
