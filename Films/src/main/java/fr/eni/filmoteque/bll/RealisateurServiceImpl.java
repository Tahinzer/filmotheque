package fr.eni.filmoteque.bll;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.eni.filmoteque.bo.Realisateur;
import fr.eni.filmoteque.dal.RealisateurDAO;

@Service("realisateurService")
public class RealisateurServiceImpl implements RealisateurService{

	private RealisateurDAO realisateurDAO; 
	
	public RealisateurServiceImpl(RealisateurDAO realisateurDAO) {
		this.realisateurDAO=realisateurDAO;
	}
	
	@Override
	public List<Realisateur> getListeReal() {
		
		return realisateurDAO.findAll();
	}

	@Override
	public Realisateur findRealById(Integer id) {
		// TODO Auto-generated method stub
		return realisateurDAO.read(id);
	}

}
