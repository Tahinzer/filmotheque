package fr.eni.filmoteque.bll;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eni.filmoteque.bo.Film;
import fr.eni.filmoteque.dal.FilmDAO;
import jakarta.validation.Valid;

@Service
public class FilmServiceImpl implements FilmService {

	@Autowired
	private FilmDAO filmDAO;


	public FilmServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	public FilmServiceImpl(FilmDAO filmDAO) {
		this.filmDAO = filmDAO;
	}

	@Override
	public List<Film> getFilms() {
		return filmDAO.findAll();
	}

	@Override
	public Film findByID(Integer id) {

		return filmDAO.read(id);

	}

	@Override
	public void ajouterFilm( Film film) {
		filmDAO.save(film);
		
	}

	

}
