package fr.eni.filmoteque.bll;

import java.util.List;

import fr.eni.filmoteque.bo.Realisateur;

public interface RealisateurService {

	public List<Realisateur>getListeReal();
	public Realisateur findRealById(Integer id);
}
