package fr.eni.filmoteque.bll;

import java.util.List;

import fr.eni.filmoteque.bo.Film;
import jakarta.validation.Valid;

public interface FilmService {

	List<Film> getFilms();
	
	Film findByID(Integer id);

	void ajouterFilm(@Valid Film film);
	
}
