package fr.eni.filmoteque.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import fr.eni.filmoteque.bll.RealisateurServiceImpl;
import fr.eni.filmoteque.bo.Realisateur;

@Component
public class StringToRealController implements Converter<String, Realisateur>{

	private RealisateurServiceImpl service;
	
	@Autowired
	public void setRealisateurService(RealisateurServiceImpl service) {
		this.service=service;
	}
	
	@Override
	public Realisateur convert(String id) {
		Integer theid =Integer.parseInt(id);
		return service.findRealById(theid);
	}

}
