package fr.eni.filmoteque.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


import fr.eni.filmoteque.bll.GenreServiceImpl;
import fr.eni.filmoteque.bo.Genre;

@Component
public class StringToGenreController implements Converter<String, Genre>{

	private GenreServiceImpl service;
	
	@Autowired
	public void setGenreService(GenreServiceImpl service) {
		this.service=service;
	}
	
	@Override
	public Genre convert(String id) {
		Integer theid =Integer.parseInt(id);
		return service.findGenreById(theid);
	}




}
