package fr.eni.filmoteque.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import fr.eni.filmoteque.bll.FilmService;
import fr.eni.filmoteque.bll.GenreService;
import fr.eni.filmoteque.bll.RealisateurService;
import fr.eni.filmoteque.bo.Film;
import jakarta.validation.Valid;
@Controller
public class FilmController {

	private FilmService filmService;
	private GenreService genreService;
	private RealisateurService realisateurService;
	
	public FilmController(FilmService filmService, GenreService genreService, RealisateurService realisateurService) {
		this.filmService=filmService;
		this.genreService=genreService;
		this.realisateurService=realisateurService;
	}

	@GetMapping({"/","/films"})
	public String afficherFilms(Model model) {
		
		List<Film>lstFilms = filmService.getFilms();
		
		model.addAttribute("films", lstFilms);
		return "view-films";
	}
	
	@GetMapping("/details")
	public String detailFilm(Integer id, Model model ) {
		
		Film film = filmService.findByID(id);
		model.addAttribute("film", film);
		return "view-details";
	}
	
	@GetMapping("/VueAjouter")
	public String  VueAjouterFilm( @ModelAttribute Film film) {
		
		return "view-ajouter";
	}
	
	
	
	@PostMapping("/ajouter")
	public String  ajouterFilm(@Valid @ModelAttribute Film film, BindingResult validationResult) {
		
		if (validationResult.hasErrors()) {
			return "view-ajouter";
		}
		
		filmService.ajouterFilm(film);
		
		return "redirect:/films";
	}
	
	@GetMapping("/avis")
	public String avisFilm(Integer id, Model model ) {
		
		Film film = filmService.findByID(id);
		model.addAttribute("film", film);
		return "view-avis";
	}
	
}
