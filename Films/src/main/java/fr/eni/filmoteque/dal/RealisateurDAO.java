package fr.eni.filmoteque.dal;

import java.util.List;

import fr.eni.filmoteque.bo.Realisateur;

public interface RealisateurDAO {

	List<Realisateur>findAll();
	Realisateur read(Integer idReal);
}
