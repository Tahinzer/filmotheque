package fr.eni.filmoteque.dal;

import java.util.List;

import fr.eni.filmoteque.bo.Genre;

public interface GenreDAO {

	List<Genre>findAll();
	Genre read(Integer idGenre);
}
