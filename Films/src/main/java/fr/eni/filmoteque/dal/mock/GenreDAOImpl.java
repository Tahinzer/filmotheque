package fr.eni.filmoteque.dal.mock;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import fr.eni.filmoteque.bo.Genre;
import fr.eni.filmoteque.dal.GenreDAO;

@Repository
@Primary
public class GenreDAOImpl implements GenreDAO {

	
	
	private static final String SELECT_ALL ="select id, libelle from GENRE";
	private static final String SELECT_BY_ID="select * from GENRE where id=:id";
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate=namedParameterJdbcTemplate;
	}
	
	
	
	
	@Override
	public List<Genre> findAll() {
		List<Genre> genres;
		genres = namedParameterJdbcTemplate.query(SELECT_ALL, new BeanPropertyRowMapper<>(Genre.class));
		return genres;
	}

	@Override
	public Genre read(Integer id) {
		Genre src = new Genre(id);
		Genre genre = namedParameterJdbcTemplate.queryForObject(SELECT_BY_ID, new BeanPropertySqlParameterSource(src), new BeanPropertyRowMapper<>(Genre.class));
		return genre;
	}

}
