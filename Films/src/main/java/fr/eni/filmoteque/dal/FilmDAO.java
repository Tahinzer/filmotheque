package fr.eni.filmoteque.dal;

import java.util.List;

import fr.eni.filmoteque.bo.Film;

public interface FilmDAO {

	List<Film> findAll();
	
	Film read(Integer id);

	void save(Film film);
}
