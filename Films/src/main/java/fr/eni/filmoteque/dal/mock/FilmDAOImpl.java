package fr.eni.filmoteque.dal.mock;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import fr.eni.filmoteque.bo.Film;
import fr.eni.filmoteque.dal.FilmDAO;
import fr.eni.filmoteque.dal.GenreDAO;
import fr.eni.filmoteque.dal.RealisateurDAO;

@Repository
@Primary
public class FilmDAOImpl implements FilmDAO{

	private static final String SELECT_ALL="select id, nom, annee, resume_, id_genre, id_personne, bandeAnnonce, image_ from FILMS";
	private static final String SELECT_BY_NAME ="select * from FILMS where id = :id";
	private static final String INSERT ="insert into FILMS (  nom, annee, resume_, id_genre, id_personne, bandeAnnonce, image_) values (  :nom, :annee, :resume_, :id_genre, :id_realisateur, :bandeAnnonce, :image_)" ;
	
	
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private GenreDAO genreDAO;
	private RealisateurDAO realisateurDAO;
	
	
	@Autowired
	public void setGenreDAO(GenreDAO genreDAO) {
		this.genreDAO = genreDAO;
	}

	@Autowired
	public void setRealisateurDAO(RealisateurDAO realisateurDAO) {
		this.realisateurDAO = realisateurDAO;
	}

	@Autowired
	public void setNamedParameterJdbcTemplate( NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate=namedParameterJdbcTemplate;
	}
	
	@Override
	public List<Film> findAll() {
		List<Film> films;
		films = namedParameterJdbcTemplate.query(SELECT_ALL, new RowMapper<Film>() {

			@Override
			public Film mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				return new Film(rs.getInt("id"), rs.getString("nom"), rs.getInt("annee"),rs.getString("resume_"),genreDAO.read(rs.getInt("id_genre")), realisateurDAO.read(rs.getInt("id_personne")),rs.getString("bandeAnnonce"), rs.getString("image_"));
			}
			
		
		});
		return films;
	}

	@Override
	public Film read(Integer id) {
		Film src = new Film(id);
		Film film = namedParameterJdbcTemplate.queryForObject(SELECT_BY_NAME, new BeanPropertySqlParameterSource(src), new RowMapper<Film>() {

			@Override
			public Film mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				return new Film( rs.getInt("id"),rs.getString("nom"), rs.getInt("annee"),rs.getString("resume_"),genreDAO.read(rs.getInt("id_genre")), realisateurDAO.read(rs.getInt("id_personne")),rs.getString("bandeAnnonce"), rs.getString("image_"));
			}
		
		});
		
		return film;
	}

	@Override
	public void save(Film film) {
		
		
			Film newFilm = new Film(film.getNom(), film.getAnnee(), film.getResume_(),film.getGenre(),film.getRealisateur(), film.getId_genre(),film.getId_realisateur(), film.getBandeAnnonce(), film.getImage_());
			KeyHolder keyHolder = new GeneratedKeyHolder();
			namedParameterJdbcTemplate.update(INSERT, 
					new BeanPropertySqlParameterSource(newFilm),					
					keyHolder
					);
			newFilm.setId( keyHolder.getKey().intValue());
			System.out.println("insert todo : " + film);
		}
	
	//findAllByFilm
	}


