package fr.eni.filmoteque.dal.mock;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import fr.eni.filmoteque.bo.Realisateur;
import fr.eni.filmoteque.dal.RealisateurDAO;

@Repository
@Primary
public class RealisateurDAOImpl  implements RealisateurDAO{

	
	
	private static final String SELECT_ALL ="select id, nom from PERSONNE";
	private static final String SELECT_BY_ID ="select * from PERSONNE where id =:id";
	
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate=namedParameterJdbcTemplate;
	}
	
	
	@Override
	public List<Realisateur> findAll() {
		List<Realisateur> realisateurs;
		realisateurs = namedParameterJdbcTemplate.query(SELECT_ALL,new BeanPropertyRowMapper<>(Realisateur.class));
		return realisateurs;
	}

	@Override
	public Realisateur read(Integer id) {
		Realisateur src= new Realisateur(id);
		Realisateur realisateur = namedParameterJdbcTemplate.queryForObject(SELECT_BY_ID, new BeanPropertySqlParameterSource(src), new BeanPropertyRowMapper<>(Realisateur.class));
		return realisateur;
	}

}
